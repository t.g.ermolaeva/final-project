package com.web.finalproject.services;

import com.web.finalproject.entities.Expenses;
import com.web.finalproject.forms.ExpensesForm;

import java.time.LocalDate;
import java.util.List;

public interface ExpensesService {
    Expenses getExpenses(Long id);

    void updateExpenses(ExpensesForm form, Long id);

    void deleteExpenses(Long id);

    List<Expenses> getExpensesByUser(Long userId);

    void addExpensesToUser(ExpensesForm form, Long userId);

    List<Expenses> getExpensesReportByUserAndByExpenseType(
            Long userId, Long expenseTypeId, LocalDate dateFrom, LocalDate dateTo);

    List<Expenses> getExpensesReportByUser(
            Long userId, LocalDate dateFrom, LocalDate dateTo);

    Double sumOfExpenses(Long userId, LocalDate dateFrom, LocalDate dateTo);

    Double sumOfExpensesByExpenseType(
            Long userId, Long expenseTypeId, LocalDate dateFrom, LocalDate dateTo);

    Double expensesOfCurrentMonth(Long userId);
}
