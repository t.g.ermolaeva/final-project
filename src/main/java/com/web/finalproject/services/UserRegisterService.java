package com.web.finalproject.services;

import com.web.finalproject.forms.UserRegisterForm;

public interface UserRegisterService {
    void registerUser(UserRegisterForm form);
}
