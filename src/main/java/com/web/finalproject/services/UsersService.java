package com.web.finalproject.services;

import com.web.finalproject.entities.User;
import com.web.finalproject.forms.UserForm;

import java.util.List;

public interface UsersService {
    List<User> getAllUsers();

    void deleteUser(Long id);

    User getUserById(Long id);

    void updateUser(Long id, UserForm form);
}
