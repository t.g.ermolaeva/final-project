package com.web.finalproject.services;

import com.web.finalproject.entities.ExpenseType;
import com.web.finalproject.forms.ExpenseTypeForm;

import java.util.List;

public interface ExpenseTypesService {
    List<ExpenseType> getAll();

    ExpenseType getExpenseTypeById(Long id);

    void addExpenseType(ExpenseTypeForm form);

    void deleteExpenseType(Long id);

    void updateExpenseType(ExpenseTypeForm form, Long id);
}
