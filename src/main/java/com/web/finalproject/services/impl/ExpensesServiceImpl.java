package com.web.finalproject.services.impl;

import com.web.finalproject.entities.Expenses;
import com.web.finalproject.entities.User;
import com.web.finalproject.forms.ExpensesForm;
import com.web.finalproject.repositories.ExpensesRepository;
import com.web.finalproject.repositories.UsersRepository;
import com.web.finalproject.services.ExpensesService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
@Service
public class ExpensesServiceImpl implements ExpensesService {

    private final ExpensesRepository expensesRepository;
    private final UsersRepository usersRepository;

    @Override
    public Expenses getExpenses(Long id) {
        return expensesRepository.getById(id);
    }

    @Override
    public void deleteExpenses(Long id) {
        expensesRepository.deleteById(id);
    }

    @Override
    public void updateExpenses(ExpensesForm form, Long id) {
        Expenses expenses = expensesRepository.getById(id);
        expenses.setDate(form.getDate());
        expenses.setExpenseType(form.getExpenseType());
        expenses.setAmount(form.getAmount());
        expenses.setNote(form.getNote());

        expensesRepository.save(expenses);
    }

    @Override
    public List<Expenses> getExpensesByUser(Long userId) {
        return expensesRepository.findFirst10ByUserIdOrderByDateDesc(userId);
    }

    @Override
    public void addExpensesToUser(ExpensesForm form, Long userId) {
        User user = usersRepository.getById(userId);

        Expenses expenses = Expenses.builder()
                .date(form.getDate())
                .expenseType(form.getExpenseType())
                .amount(form.getAmount())
                .note(form.getNote())
                .build();

        expenses.setUser(user);
        expensesRepository.save(expenses);
    }

    @Override
    public List<Expenses> getExpensesReportByUserAndByExpenseType(
            Long userId, Long expenseTypeId, LocalDate dateFrom, LocalDate dateTo) {
        return expensesRepository
                .findAllByUserIdAndExpenseTypeIdAndDateBetweenOrderByDateDesc(userId, expenseTypeId, dateFrom, dateTo);
    }

    @Override
    public List<Expenses> getExpensesReportByUser(Long userId, LocalDate dateFrom, LocalDate dateTo) {
        return expensesRepository.findAllByUserIdAndDateBetweenOrderByDateDesc(userId, dateFrom, dateTo);
    }

    @Override
    public Double sumOfExpenses(Long userId, LocalDate dateFrom, LocalDate dateTo) {
        return expensesRepository.findAllByUserIdAndDateBetween(userId, dateFrom, dateTo)
                .stream()
                .mapToDouble(Expenses::getAmount)
                .sum();
    }

    @Override
    public Double sumOfExpensesByExpenseType(
            Long userId, Long expenseTypeId, LocalDate dateFrom, LocalDate dateTo) {
        return expensesRepository
                .findAllByUserIdAndExpenseTypeIdAndDateBetween(userId, expenseTypeId, dateFrom, dateTo)
                .stream()
                .mapToDouble(Expenses::getAmount)
                .sum();
    }

    @Override
    public Double expensesOfCurrentMonth(Long userId) {
        LocalDate dateTo = LocalDate.now();
        LocalDate dateFrom = dateTo.withDayOfMonth(1);
        return expensesRepository.findAllByUserIdAndDateBetween(userId, dateFrom, dateTo)
                .stream()
                .mapToDouble(Expenses::getAmount)
                .sum();
    }
}
