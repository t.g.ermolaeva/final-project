package com.web.finalproject.services.impl;

import com.web.finalproject.entities.ExpenseType;
import com.web.finalproject.forms.ExpenseTypeForm;
import com.web.finalproject.repositories.ExpenseTypesRepository;
import com.web.finalproject.services.ExpenseTypesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExpenseTypesServiceImpl implements ExpenseTypesService {

    private final ExpenseTypesRepository expenseTypesRepository;

    @Autowired
    public ExpenseTypesServiceImpl(ExpenseTypesRepository expenseTypesRepository) {
        this.expenseTypesRepository = expenseTypesRepository;
    }

    @Override
    public List<ExpenseType> getAll() {
        return expenseTypesRepository.findAll();
    }

    @Override
    public ExpenseType getExpenseTypeById(Long id) {
        return expenseTypesRepository.getById(id);
    }

    @Override
    public void addExpenseType(ExpenseTypeForm form) {
        ExpenseType expenseType = ExpenseType.builder()
                .name(form.getName())
                .build();

        expenseTypesRepository.save(expenseType);
    }

    @Override
    public void deleteExpenseType(Long id) {
        expenseTypesRepository.deleteById(id);
    }

    @Override
    public void updateExpenseType(ExpenseTypeForm form, Long id) {
        ExpenseType expenseType = expenseTypesRepository.getById(id);
        expenseType.setName(form.getName());

        expenseTypesRepository.save(expenseType);
    }
}
