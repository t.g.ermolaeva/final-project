package com.web.finalproject.forms;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class IncomeTypeForm {

    @NotNull
    @Length(max = 20)
    private String name;
}
