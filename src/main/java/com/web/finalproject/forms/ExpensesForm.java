package com.web.finalproject.forms;

import com.web.finalproject.entities.ExpenseType;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class ExpensesForm {

    @NotNull
    private LocalDate date;

    @NotNull
    private ExpenseType expenseType;

    @NotNull
    private Double amount;

    @NotEmpty
    @Length(max = 50)
    private String note;
}
