package com.web.finalproject.forms;

import com.web.finalproject.entities.IncomeType;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class IncomesForm {

    @NotNull
    private LocalDate date;

    @NotNull
    private IncomeType incomeType;

    @NotNull
    private Double amount;

    @NotEmpty
    @Length(max = 50)
    private String note;
}
