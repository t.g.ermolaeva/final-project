package com.web.finalproject.controller;

import com.web.finalproject.forms.UserRegisterForm;
import com.web.finalproject.services.UserRegisterService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequiredArgsConstructor
@Controller
@RequestMapping("/register")
public class UserRegisterController {

    private final UserRegisterService userRegisterService;

    @GetMapping
    public String getRegisterPage() {
        return "register";
    }

    @PostMapping
    public String registerUser(UserRegisterForm form) {
        userRegisterService.registerUser(form);
        return "redirect:/login";
    }

}
