package com.web.finalproject.controller;

import com.web.finalproject.services.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequiredArgsConstructor
@Controller
@RequestMapping("/users")
public class UsersController {

    private final UsersService usersService;

    @GetMapping
    public String getUsersPage(Model model) {
        model.addAttribute("users", usersService.getAllUsers());
        return "users";
    }

    @PostMapping("/delete")
    public String deleteUser(@RequestParam("userId") Long id) {
        usersService.deleteUser(id);
        return "redirect:/users";
    }
}
