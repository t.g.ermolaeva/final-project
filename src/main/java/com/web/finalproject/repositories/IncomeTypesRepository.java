package com.web.finalproject.repositories;

import com.web.finalproject.entities.IncomeType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IncomeTypesRepository extends JpaRepository<IncomeType, Long> {
}
